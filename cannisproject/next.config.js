/** @type {import('next').NextConfig} */
const withAntdLess = require('next-plugin-antd-less');
const antdVariables = require('./styles/antd_variables');


const nextConfig = withAntdLess({
  modifyVars: antdVariables,
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ['cannis-catering.online'],
  },
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
})

module.exports = nextConfig
