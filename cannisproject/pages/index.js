import LandingPage from "../components/CustomerLayout/page/LandingPage";
import LandingPageIndex from "../components/CustomerLayout/LandingPageIndex";
import SecondLandingPage from "../components/CustomerLayout/page/SecondLandingPage";
import { Layout } from "antd";
import WhyUsPage from "../components/CustomerLayout/page/WhyUsPage";
import ThirdLandingPage from "../components/CustomerLayout/page/ThirdLandingPage";

const Beranda = () => (
  <>
    <LandingPageIndex>
      <Layout className=" bg-transparent h-full mx-28 my-10">
        <LandingPage />
      </Layout>
      <Layout className=" bg-mainColor h-full">
        <Layout className="bg-transparent mx-28 my-10">
          <WhyUsPage />
        </Layout>
      </Layout>
      <Layout className=" bg-amber-50 h-full">
        <Layout className="bg-transparent mx-28 my-24">
          <SecondLandingPage />
        </Layout>
      </Layout>
      <Layout className=" bg-mainColor ">
        <Layout className="bg-transparent mx-28 my-10">
          <ThirdLandingPage />
        </Layout>
      </Layout>
    </LandingPageIndex>
  </>
);

export default Beranda;
