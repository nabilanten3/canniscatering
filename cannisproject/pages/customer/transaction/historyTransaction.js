import { Layout } from "antd";
import React from "react";
import CustomerLayout from "../../../components/CustomerLayout/CustomerLayout";
import TransactionTable from "../../../components/CustomerLayout/historyTransaction/TransactionTable";

const historyTransaction = () => {
  return (
    <CustomerLayout>
      <Layout className=" bg-blue-100 h-screen mx-28 mt-10 px-2">
        <TransactionTable />
      </Layout>
    </CustomerLayout>
  );
};

export default historyTransaction;
