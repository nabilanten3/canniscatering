import { Layout } from "antd";
import React from "react";
import CustomerLayout from "../../../components/CustomerLayout/CustomerLayout";
import ShippingAddress from "../../../components/CustomerLayout/currentTransaction/ShippingAddress";
import UserShoppingDetails from "../../../components/CustomerLayout/currentTransaction/UserShoppingDetails";
import PaymentMethod from "../../../components/CustomerLayout/currentTransaction/PaymentMethod";

const Transaction = () => {
  return (
    <CustomerLayout>
      <Layout className=" bg-slate-50 h-screen mx-28 mt-10 px-2">
        <ShippingAddress />
        <UserShoppingDetails />
        <PaymentMethod />
      </Layout>
    </CustomerLayout>
  );
};

export default Transaction;
