import React from "react";
import { Layout, Tabs } from "antd";
const { TabPane } = Tabs;
import CustomerLayout from "../../../components/CustomerLayout/CustomerLayout";
import TransactionTable from "../../../components/CustomerLayout/historyTransaction/TransactionTable";
import CurrentTransaction from "../../../components/CustomerLayout/currentTransaction";

const Transaction = () => {
  return (
    <CustomerLayout>
      <Layout className="mx-28 bg-transparent">
        <Tabs className="shadow-lg">
          <TabPane tab="Current Transaction" key="currentTransaction">
            <CurrentTransaction />
          </TabPane>
          <TabPane tab="Transaction History" key="historyTransaction">
            <TransactionTable />
          </TabPane>
        </Tabs>
      </Layout>
    </CustomerLayout>
  );
};

export default Transaction;
