import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Layout, Button, Result, Tabs } from "antd";
const { TabPane } = Tabs;
import CustomerLayout from "../../../components/CustomerLayout/CustomerLayout";
import axios from "axios";
import CurrentTransaction from "../../../components/CustomerLayout/currentTransaction/CurrentTransaction";
import jwt_decode from "jwt-decode";
import TransactionTable from "../../../components/CustomerLayout/historyTransaction/TransactionTable";

const transaction = () => {
  const router = useRouter();
  const [transaction, setTransaction] = useState();
  const { userId } = router.query;
  const [fullName, setFullName] = useState();
  const [address, setAddress] = useState();
  const [phone, setPhone] = useState();
  const [deliveryStatus, setDeliveryStatus] = useState();
  const [menuName, setMenuName] = useState();
  const [menuPrice, setMenuPrice] = useState("");
  const [menuId, setMenuId] = useState("");
  const [statusPayment, setStatusPayment] = useState("")
  const [date, setDate] = useState()
  const [idTransaction, setIdTransaction] = useState("")

  // const getTokenJwt = async () => {
  //     const tokenUser = localStorage.getItem("access_token")
  //     const decodedToken = jwt_decode(tokenUser)
  //     // console.log(decodedToken, "decode an jwt")
  //     setIdUser(decodedToken.id)
  // }

  const getUser = async () => {
    try {
      let transaction_id = localStorage.getItem("transaction_id");
      if (!transaction_id) {
        setTransaction(false);
      } else {
        setTransaction(true);
        setIdTransaction(transaction_id)
      }
      let response = await axios.get(
        `http://cannis-catering.online:3000/users/${userId}`, { headers: { "content-type": "application/json", Authorization: `Bearer ${localStorage.getItem("access_token")}` } }
      );
      let responseTransaction = await axios.get(
        `http://cannis-catering.online:3000/users/history/${userId}`, { headers: { "content-type": "application/json", Authorization: `Bearer ${localStorage.getItem("access_token")}` } }
      );
      let responseCurrentTransaction = await axios.get(
        `http://cannis-catering.online:3000/transactions/checkout/${transaction_id}`, { headers: { "content-type": "application/json", Authorization: `Bearer ${localStorage.getItem("access_token")}` } }
      );
      // console.log(
      //   responseCurrentTransaction.data.data,
      //   "Current Transaction id"
      // );
      // console.log(responseTransaction, "ini dari ResponseTransaction")
      // console.log(response.data.data, 'dari si getUser Response')
      setDeliveryStatus(responseCurrentTransaction.data.data.deliveryStatus);
      setStatusPayment(responseCurrentTransaction.data.data.paymentStatus)
      setMenuName(responseCurrentTransaction.data.data.menu.menu);
      setMenuPrice(responseCurrentTransaction.data.data.menu.price);
      setMenuId(responseCurrentTransaction.data.data.menu.id);
      setFullName(response.data.data.fullName);
      setAddress(response.data.data.address);
      setPhone(response.data.data.phone);
      setDate(responseCurrentTransaction.data.data.createdAt.split("T")[0])
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    getUser();
  }, []);

  // const getCurrentTransaction = async () => {

  //     try {
  //         let response = await axios.get(`http://cannis-catering.online:3000/users/history/${idUser}`)
  //         console.log(response, "ini response dari getCurrentTransaction");

  //     } catch (e) { console.log(e.message, "ini response eror dari getCurrentTransaction") }
  // }

  // useEffect(() => {
  //     getCurrentTransaction()

  // }, [])

  return (
    <CustomerLayout>
      <Layout className="bg-transparent mx-28 mt-10 px-2">
        <Tabs>
          <TabPane
            tab="Current Transaction"
            key="currentTransaction"
            className="min-h-screen"
          >
            {transaction ? (
              <CurrentTransaction
                userDetails={{
                  fullName,
                  address,
                  phone,
                  getUser,
                  deliveryStatus,
                  menuName,
                  menuPrice,
                  menuId,
                  statusPayment,
                  date,
                  idTransaction
                }}
              />
            ) : (
              <Result
                status="info"
                title="No Transaction"
                extra={
                  <Button type="dashed" key="console" onClick={() => router.push("/customer/product")}>
                    Buy Diet Plan
                  </Button>
                }
              />
            )}
          </TabPane>
          <TabPane
            tab="Transaction History"
            key="historyTransaction"
            className="min-h-screen"
          >
            <TransactionTable />
          </TabPane>
        </Tabs>
      </Layout>
    </CustomerLayout>
  );
};

export default transaction;
