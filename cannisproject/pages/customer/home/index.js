import { Layout } from "antd";
import React from "react";
import CustomerLayout from "../../../components/CustomerLayout/CustomerLayout";
import LandingPage from "../../../components/CustomerLayout/page/LandingPage";
import SecondLandingPage from "../../../components/CustomerLayout/page/SecondLandingPage";
import ThirdLandingPage from "../../../components/CustomerLayout/page/ThirdLandingPage";
import WhyUsPage from "../../../components/CustomerLayout/page/WhyUsPage";

const Home = () => {
  const contentStyle = {
    height: '100%',
    minHeight: '100vh',
    maxHeight: '200vh',
    lineHeight: '100px',
    textAlign: 'center',

  };
  return (
    <CustomerLayout>
      <Layout className=" bg-transparent h-full mx-28 my-10">
        <LandingPage />
      </Layout>
      <Layout className=" bg-mainColor h-full">
        <Layout className="bg-transparent mx-28 my-10">
          <WhyUsPage />
        </Layout>
      </Layout>
      <Layout className=" bg-amber-50 h-full">
        <Layout className="bg-transparent mx-28 my-24">
          <SecondLandingPage />
        </Layout>
      </Layout>
      <Layout className=" bg-mainColor ">
        <Layout className="bg-transparent mx-28 my-10">
          <ThirdLandingPage />
        </Layout>
      </Layout>
    </CustomerLayout>
  );
};

export default Home;
