import React from "react";
import { Row, Col } from "antd";
import DetailMenu from "../components/CustomerLayout/Menu/DetailMenu";
import DetailsSnacks from "../components/CustomerLayout/Menu/DetailsSnacks";

const coba2 = () => {
    return (
        <Row>
            <Col span={6} offset={12}>
                <h1 className="font-bold text-3xl">Main Menu</h1>
                <DetailMenu />
                <h1 className="font-bold text-3xl">Snacks</h1>
                <DetailsSnacks />
            </Col>
        </Row>
    );
};

export default coba2;
