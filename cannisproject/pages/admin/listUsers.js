import React from "react";
import AdminLayout from "../../components/AdminLayout/AdminLayout";
import AdminUsers from "../../components/AdminLayout/AdminUsers";

const ListUsers = () => {
  return (
    <AdminLayout>
      <AdminUsers />
    </AdminLayout>
  );
};

export default ListUsers;
