import React from "react";
import AdminDashboard from "../../components/AdminLayout/AdminDashboard";
import AdminLayout from "../../components/AdminLayout/AdminLayout";

const Dashboard = () => {
  return (
    <AdminLayout>
      <AdminDashboard />
    </AdminLayout>
  );
};

export default Dashboard;
