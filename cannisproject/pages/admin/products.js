import React from "react";
import AdminLayout from "../../components/AdminLayout/AdminLayout";
import AdminProducts from "../../components/AdminLayout/AdminProducts";

const Products = () => {
  return (
    <AdminLayout>
      <AdminProducts />
    </AdminLayout>
  );
};

export default Products;
