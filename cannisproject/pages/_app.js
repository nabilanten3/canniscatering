import "../styles/globals.css";
// import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import "antd/dist/antd.variable.css";
import "tailwindcss/tailwind.css";
import { ConfigProvider } from "antd";
ConfigProvider.config({
  theme: {
    primaryColor: "#95CB44",
  },
});
function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
