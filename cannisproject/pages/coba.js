import React, { useContext } from "react";
import { MenuContext, MenuProvider } from "../components/context/menuContext";
import { Button, Col, message, Row, Menu, Dropdown, Card, Table } from "antd";
import Image from "next/image";
import { DownOutlined } from "@ant-design/icons";
import FormAddProduct from "../components/AdminLayout/FormAddProduct";

const Menus = () => {
  const { menu } = useContext(MenuContext);
  const menus = (
    <Menu
      onClick={(e) => {
        console.log(e.key);
        setTimePeriod(e.key);
        if (e.key === "Per Month") {
          setPrice("Rp.500.000");
        } else {
          setPrice("Rp.200.000");
        }
      }}
      items={[
        { label: "Per Week", key: "Per Week" },
        { label: "Per Month", key: "Per Month" },
      ]}
    />
  );

  const colums = [{ label: "title", dataIndex: "title", key: "title" }]

  return (
    // <div>
    //   {/* {menu.map((menu) => (
    //     <img src={menu.thumbnailUrl} />
    //   ))} */}

    //   {menu.title}
    // </div>
    <>
      <Table dataSource={menu} columns={colums} />
    </>
  );
};

const coba = () => {
  return (
    <div>
      {/* <MenuProvider>
        <Menus />
        <Menus />
        <Menus />
      </MenuProvider> */}
      <FormAddProduct />
    </div>
  );
};

export default coba;
