import React, { createContext, useEffect, useState } from "react";
import axios from "axios";

const MenuContext = createContext(null);

const MenuProvider = ({ children }) => {
  const [menu, setMenu] = useState([]);
  const getMenus = async () => {
    let response = await axios.get(
      "https://c2db-101-255-119-166.ngrok.io/menu"
    );
    console.log(response.data.items);
    setMenu(response.data.items);
  };

  useEffect(() => {
    getMenus();
  }, []);

  return (
    <MenuContext.Provider value={{ menu, setMenu }}>{children}</MenuContext.Provider>
  );
};

export { MenuContext, MenuProvider };
