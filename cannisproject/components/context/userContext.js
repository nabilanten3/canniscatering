import React, { createContext, useEffect, useState } from "react";
import axios from "axios";

const UserContext = createContext(null);

const UserProvider = ({ children }) => {
  const [user, setUser] = useState([]);

  const getUsers = async () => {
    let response = await axios.get(
      "https://669b-101-255-119-166.ngrok.io/users"
    );
    console.log(response.data.items);
    setUser(response.data.items);
  };

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <UserContext.Provider value={{ user }}>{children}</UserContext.Provider>
  );
};

export { UserContext, UserProvider };
