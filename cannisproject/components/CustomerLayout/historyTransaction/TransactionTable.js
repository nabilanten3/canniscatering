import { Table, Tag } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";
import jwtDecode from "jwt-decode";

const columns = [
  { title: "Date", render: (value) => value.createdAt.split("T")[0] },
  { title: "Name", render: (value) => value.user.fullName },
  { title: "Email", render: (value) => value.user.email },
  { title: "Phone", render: (value) => value.user.phone },
  { title: "Menu", render: (value) => value.menu.menu },
  { title: "Total Price", render: (value) => <span>Rp. {value.total} </span> },
  { title: "Delivery Status", render: (value) => <Tag color="lime">{value.deliveryStatus}</Tag> },
];

const TransactionTable = () => {
  const [dataSource, setDataSource] = useState([]);
  const getUser = async () => {
    const userToken = localStorage.getItem("access_token");
    const decodedToken = jwtDecode(userToken);
    let response = await axios.get(`http://cannis-catering.online:3000/users/`, { headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` } });
    const users = response.data.items;
    const selectedUser = users.find((user) => user.id === decodedToken.id);
    const idUser = selectedUser.id;
    let historyTransactionResponse = await axios.get(
      `http://cannis-catering.online:3000/users/history/${idUser}`, { headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` } }
    );
    console.log(historyTransactionResponse.data, "dari history");
    setDataSource(historyTransactionResponse.data);
  };

  useEffect(() => {
    getUser();
  }, []);
  return (
    <div>
      <Table dataSource={dataSource} columns={columns} />
    </div>
  );
};

export default TransactionTable;
