import { HomeFilled, SettingFilled, LogoutOutlined } from "@ant-design/icons";
import { Button, Layout, Menu } from "antd";
import React, { useState } from "react";
const { Sider } = Layout;
import Link from "next/link";

function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}

const items = [
  getItem(<Link href={"/customer/home"}>Home</Link>, "home", <HomeFilled />),
  getItem("Setting", "setting", <SettingFilled />),
  getItem(<Button type="text" danger onClick={(e) => console.log(e)}>Delete Account</Button>, "deleteAccount", <LogoutOutlined style={{ color: "red" }} />),
];

const SideBar = () => {
  const getTokenJwt = async () => {
    try {
      const getToken = localStorage.getItem("access_token")
      const decodedToken = await jwt_decode(getToken)
      const response = await axios.get(`http://cannis-catering.online:3000/users/${decodedToken.id}`, { headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` } })
      console.log(response.data.data, "ini dari response")
    } catch (e) { console.log(e.message) }

  }
  const [collapsed, setCollapsed] = useState(false);
  return (
    <Sider
      collapsible
      collapsed={collapsed}
      onCollapse={(value) => setCollapsed(value)}
      theme={"light"}
    >
      <div className="logo" />
      <Menu
        theme="light"
        defaultSelectedKeys={["setting"]}
        mode="inline"
        items={items}
      />
    </Sider>
  );
};

export default SideBar;
