import React, { useEffect, useState } from "react";
import { Card, Col, Row } from "antd";
import axios from "axios";
import Image from "next/image";

const DetailsSnacks = () => {
  const [menus, setMenus] = useState([]);
  const getMenu = async () => {
    try {
      await axios
        .get("http://cannis-catering.online:3000/menu")
        .then((res) => setMenus(res.data.items));
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    getMenu();
  }, []);

  return (
    <div className="mx-3 my-5">
      <div className="my-10">
        <Row justify={"space-around"}>
          {menus.map((menu, key) => {
            // console.log("ini dari jsx", menu.category.id);
            if (menu.category.id === "c4bef283-49dd-4081-9c61-88a45666495f") {
              return (
                <Col
                  key={key}
                  span={7}
                  className="my-5 duration-200 hover:-translate-y-6"
                >
                  <Card hoverable className="shadow-sm">
                    <Image
                      src={`http://cannis-catering.online:3000/file/${menu.image}`}
                      width={300}
                      height={250}
                    />
                    <h1 className="text-md font-bold">{menu.menu}</h1>
                  </Card>
                </Col>
              );
            }
          })}
        </Row>
      </div>
    </div>
  );
};

export default DetailsSnacks;
