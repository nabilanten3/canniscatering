import React, { useState, useEffect } from "react";
import { Col, message, Row } from "antd";
import axios from "axios";
import Image from "next/image";
import ReactMarkdown from "react-markdown";

const DetailsProducts = () => {
  const [menus, setMenus] = useState([]);
  const getMenu = async () => {
    try {
      await axios
        .get("http://cannis-catering.online:3000/menu")
        .then((res) => setMenus(res.data.items));
    } catch (e) {
      console.log(e.message);
    }
  };
  useEffect(() => {
    getMenu();
  }, []);

  return (
    <div className="mx-3 my-5">
      {menus.map((menu, key) => {
        const handleBuyProduct = async () => {
          message.warn("You Must Login Before Buy");
        };
        if (menu.category.id === "729006e4-0a9b-40a5-8e65-e0bab714c1ac") {
          return (
            <div className="my-10" key={key}>
              <Row gutter={16}>
                <Col>
                  <Image
                    src={`http://cannis-catering.online:3000/file/${menu.image}`}
                    width={450}
                    height={350}
                  />
                </Col>
                <Col>
                  <h1 className="text-2xl font-bold mb-2">{menu.menu}</h1>
                  <p className="text-xl font-medium text-green-600 mb-1">
                    Rp.{menu.price}/Week
                  </p>
                  <p className="text-lg mb-4">
                    Description : <br />
                    <ReactMarkdown>{menu.description}</ReactMarkdown>
                  </p>
                  <button
                    onClick={handleBuyProduct}
                    className="bg-green-600 text-white rounded-md hover:bg-green-500 hover:text-white  duration-200 hover:scale-105 py-2 px-8"
                  >
                    Buy Now
                  </button>
                </Col>
              </Row>
            </div>
          );
        }
      })}
    </div>
  );
};

export default DetailsProducts;
