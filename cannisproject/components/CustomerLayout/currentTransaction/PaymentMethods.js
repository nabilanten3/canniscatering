import { Button, Steps, Col, Form, message, Modal, Rate, Row } from "antd";
const { Step } = Steps;
import { DollarOutlined, CheckCircleFilled } from "@ant-design/icons";
import TextArea from "antd/lib/input/TextArea";
import axios from "axios";
import React, { useState } from "react";
import UploadImage from "./UploadImage";

const PaymentMethods = ({ userDetails }) => {
  const [visible, setVisible] = useState(false);
  const [image, setImage] = useState();
  const [rate, setRate] = useState("");
  const [desc, setDesc] = useState("");
  const [reviewId, setReviewId] = useState("");
  const onChangeImage = (filepath) => {
    console.log(filepath, "isi dari handleChangeImage");
    setImage(filepath);
  };

  const getCheckoutTransaction = async () => {
    const transactions_id = localStorage.getItem("transaction_id");
    await axios
      .get(
        `http://cannis-catering.online:3000/transactions/checkout/${transactions_id}`,
        {
          headers: {
            "content-type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      )
      .then((res) => console.log(res.data, "dari getCheckout"))
      .then(
        setTimeout(() => {
          setVisible(true);
        }, 2000)
      );
  };

  const onFInish = async () => {
    try {
      const transactions_id = localStorage.getItem("transaction_id");
      const paymentProof = {
        menuId: userDetails.menuId,
        image: image,
        paymentStatus: false,
        deliveryStatus: "1",
      };
      await axios
        .put(
          `http://cannis-catering.online:3000/transactions/${transactions_id}`,
          paymentProof,
          {
            headers: {
              "content-type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("access_token")}`,
            },
          }
        )
        .then((res) => console.log(res, "ini dari OnFinish"))
        .then(message.success("Success Upload Payment Proof"));
    } catch (e) {
      console.log(e.message);
    }
  };

  const handleCancelTransaction = async () => {
    const transaction_id = localStorage.getItem("transaction_id");
    await axios
      .delete(
        `http://cannis-catering.online:3000/transactions/${transaction_id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      )
      .then((res) => console.log(res))
      .then(userDetails.getUser())
      .then(localStorage.removeItem("transaction_id"));
  };

  const handleDeliveryStatus = async () => {
    const transaction_id = localStorage.getItem("transaction_id");
    const deliveryStatus = {
      deliveryStatus: "2",
    };
    await axios
      .put(
        `http://cannis-catering.online:3000/transactions/admin/${transaction_id}`,
        deliveryStatus,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      )
      .then((res) => console.log(res));
    // .then(handleCancelTransaction())
  };

  const onFinishedRate = async () => {
    const reviewId = localStorage.getItem("reviewId");
    const transaction_id = localStorage.getItem("transaction_id");
    const rateMessage = {
      transaction_id: transaction_id,
      message: desc,
      rating: rate,
    };
    await axios
      .put(
        `http://cannis-catering.online:3000/review/${reviewId}`,
        rateMessage,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      )
      .then((res) => console.log(res))
      .then(setVisible(false));
  };

  const onChangeRate = (e) => {
    console.log(e);
    setRate(e);
  };

  const onChangeDesc = (e) => {
    console.log(e.target.value);
    setDesc(e.target.value);
  };

  return (
    <>
      <Row className="shadow-lg px-8 py-5 mb-8">
        <Col span={24}>
          <Row justify="space-between" className="my-5 font-bold ">
            <Col span={5}>
              <h1 className="text-lg mb-2">Payment Method</h1>
              <h1>Bank Transfer</h1>
            </Col>
            <Col span={17}>
              <h1 className="text-lg mb-2">
                Transfer to the following account number :
              </h1>
              <h2>BNI BANK : 12123342442</h2>
            </Col>
          </Row>
          <Row justify="space-between" className="my-5 font-medium">
            <Col span={24}>
              <h1>
                Please transfer to the following account number with a
                predetermined amount, no less and no more. The nominal error you
                make is not our responsibility, and then upload your proof of
                payment
              </h1>
            </Col>
          </Row>
          <Row justify="space-between" className="my-10 font-medium">
            <Col span={10}>
              <UploadImage onChangeImage={onChangeImage} />
              {userDetails.statusPayment ? (
                <Button
                  className="bg-mainColor mt-10"
                  type="primary"
                  onClick={() => handleDeliveryStatus()}
                >
                  Order Received
                </Button>
              ) : (
                <Button
                  danger
                  type="primary"
                  className="mt-10"
                  onClick={() => handleCancelTransaction()}
                >
                  Cancel Transcation
                </Button>
              )}
              {/* <Button
                danger
                className="my-10"
                type="primary"
                onClick={() => handleCancelTransaction()}
              >
                Cancel Transaction
              </Button> */}
            </Col>
            <Col span={8}>
              <h1 className=" font-bold ">
                Transaction Status :
                {userDetails.statusPayment ? (
                  <span className="text-green-600 mx-2">Approved</span>
                ) : (
                  <span className="text-red-400 mx-2">Waiting For Payment</span>
                )}
              </h1>
            </Col>
            <Col span={6}>
              <h1 className="mb-3 font-bold">Delivery Status : </h1>
              <Steps
                current={parseInt(userDetails.deliveryStatus)}
                direction="vertical"
                size="small"
              >
                <Step title="Waiting For Payment" />
                <Step title="On Delivery" />
                <Step title="Delivered" icon={<CheckCircleFilled />} />
              </Steps>
              {/* {userDetails.deliveryStatus ? (
                <button
                  onClick={() => setVisible(true)}
                  className="bg-green-600 text-white rounded-md hover:bg-green-500 hover:text-white  duration-200 hover:scale-105 py-2 px-8 "
                >
                  Give Rating
                </button>
              ) : (
                <Button onClick={() => handleDeliveryStatus()}>
                  Pesanan Diterima
                </Button>
              )} */}
            </Col>
          </Row>
          <Row justify="space-between">
            <Col></Col>
          </Row>
        </Col>
      </Row>
      <Modal
        visible={visible}
        onCancel={() => setVisible(false)}
        onOk={() => onFinishedRate()}
        closable
        centered
      >
        <Form layout="vertical" onFinish={onFinishedRate}>
          <Form.Item label="Give us Rate" name="rate">
            <Rate defaultValue={"2"} value={rate} onChange={onChangeRate} />
          </Form.Item>
          <Form.Item name="menu" label="Review">
            <TextArea onChange={onChangeDesc} value={desc} />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default PaymentMethods;
