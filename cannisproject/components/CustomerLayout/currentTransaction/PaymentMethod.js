import { Button } from "antd";
import React from "react";
import UploadImage from "./UploadImage";
import { CreditCardFilled } from "@ant-design/icons";

const PaymentMethod = () => {
  return (
    <div>
      <h1>Payment Method</h1>
      <div className=" border w-2/5 p-2 rounded-sm">
        <p className="w-full py-3 bg-green-300 text-lg text-center">
          <CreditCardFilled size={"large"} /> Bank Transfer
        </p>
        <p className="w-full  border mb-2 p-2">BNI BANK : 1122334455</p>
        <Button className="w-full mb-2 p-2">
          <UploadImage />
        </Button>
      </div>
    </div>
  );
};

export default PaymentMethod;
