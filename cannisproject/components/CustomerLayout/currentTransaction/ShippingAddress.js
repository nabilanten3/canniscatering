import React from "react";
import { Col, Row, Space } from "antd";

const ShippingAddress = ({ userDetails }) => {
  userDetails.getUser();
  return (
    <Row
      justify="space-between"
      align="middle"
      className="shadow-lg px-8 py-5 mb-8"
    >
      <Col span={3}>
        <Space direction="vertical" size={"middle"}>
          <h3>
            <b>Shipping Address</b>
          </h3>
          <p>{userDetails.fullName}</p>
          <p>{userDetails.phone}</p>
        </Space>
      </Col>
      <Col span={16}>
        <p>{userDetails.address}</p>
      </Col>
      <Col>
        <Space direction="vertical" size={"middle"}>
          <h3>
            <b>Transaction Date</b>
          </h3>
          <p>{userDetails.date}</p>
        </Space>
      </Col>
    </Row>
  );
};

export default ShippingAddress;
