import React from "react";

const LandingPageFooter = () => {
  return (
    <div className="mx-auto my-8  font-bold text-black ">
      Copyright @ 2022 Cannis Catering. All Rights Reserved
    </div>
  );
};

export default LandingPageFooter;
