import React from "react";
import { Carousel } from "antd";
import ReviewCards from "./ReviewCards";

const Reviews = () => {
  return (
    <div>
      <ReviewCards
        name="Raphiel Shanon"
        description="Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book."
      />
    </div>
  );
};

export default Reviews;
