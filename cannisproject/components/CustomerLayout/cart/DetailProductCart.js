import React, { useState } from "react";
import { Layout, Row, Col, Menu, Dropdown, Space, Button } from "antd";
import Image from "next/image";
import Lunch from "../../../public/assets/lunch.jpg";
import { DownOutlined } from "@ant-design/icons";

const DetailProductCart = () => {
  const [timePeriod, setTimePeriod] = useState("Per Week");
  const [price, setPrice] = useState("Rp.200.000");

  const menu = (
    <Menu
      onClick={(e) => {
        console.log(e.key);
        setTimePeriod(e.key);
        if (e.key === "Per Month") {
          setPrice("Rp.500.000");
        } else {
          setPrice("Rp.200.000");
        }
      }}
      items={[
        { label: "Per Week", key: "Per Week" },
        { label: "Per Month", key: "Per Month" },
      ]}
    />
  );
  return (
    <Layout className=" bg-transparent h-full mx-28 my-10">
      <Row>
        <Col>
          <Image src={Lunch} width={200} height={150} />
        </Col>
        <Col>
          <div className="mx-3">
            <p className="text-xl font-bold mb-5">Lunch Only</p>
            <p className="text-xl font-medium text-green-600 mb-2">{price}</p>
            <p className="text-xl font-medium mb-2">Time Period</p>
            <Dropdown overlay={menu} trigger={["hover"]}>
              <Button>
                {timePeriod} <DownOutlined />
              </Button>
            </Dropdown>
          </div>
        </Col>
      </Row>

      <div className="border my-5">
        <h1 className="text-lg">Description : </h1>
        <p>Lunch Only</p>
      </div>
      <Row className="my-5">
        <Col style={{ marginRight: 20 }}>
          <Button>Add Another Product</Button>
        </Col>
        <Col>
          <Button
            style={{
              backgroundColor: "green",
              color: "white",
              width: 100,
            }}
          >
            Buy
          </Button>
        </Col>
      </Row>
    </Layout>
  );
};

export default DetailProductCart;
