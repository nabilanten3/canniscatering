import { Carousel, Col, Rate, Row } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";

const ThirdLandingPage = () => {
  const [reviews, setReviews] = useState([]);
  const getReviews = async () => [
    await axios
      .get(`http://cannis-catering.online:3000/review`)
      .then((res) => setReviews(res.data.items)),
  ];

  useEffect(() => {
    getReviews();
  }, []);

  return (
    <div className="mb-16">
      <h1 className="text-4xl font-bold text-center mb-16 text-fontColor">
        Diet Warrior Experience with CannisCatering
      </h1>
      <Carousel autoplay dots={false}>
        {reviews.map((review) => {
          console.log(review);
          return (
            <>
              <Row justify="center">
                <Col span={6}>
                  <h1 className="text-xl py-2 font-bold">
                    {review.transaction.user.fullName}
                    <Rate value={review.rate} disabled className="mx-5" />
                  </h1>
                  <p>{review.message}</p>
                </Col>
              </Row>
            </>
          );
        })}
      </Carousel>
    </div>
  );
};

export default ThirdLandingPage;
