import { Layout } from "antd";
import Head from "next/head";
import React from "react";
import "tailwindcss/tailwind.css";
import AuthorizedNavigation from "./CustomerLayoutComponents/AuthorizedNavigation";
import LandingPageFooter from "./CustomerLayoutComponents/LandingPageFooter";

const CustomerLayout = ({ children }) => {
  return (
    <Layout className=" bg-amber-50 font-sans">
      <Head>
        <title>Cannis Catering</title>
      </Head>
      <Layout className=" bg-transparent mx-28">
        <AuthorizedNavigation />
      </Layout>
      {children}
      <Layout className=" bg-amber-50 h-full">
        <LandingPageFooter />
      </Layout>
    </Layout>
  );
};

export default CustomerLayout;
