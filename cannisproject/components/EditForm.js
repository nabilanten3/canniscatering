import React, { useEffect, useState } from "react";
import { Form, Select, Option, Input, Button } from "antd";
import axios from "axios";

const EditForm = () => {
  const [menu, setMenu] = useState([]);
  const [form] = Form.useForm();

  const mappedMenu = menu.map((menu) => {
    const nyobj = {
      category: menu.category.category,
      menu: menu.menu,
      price: menu.price,
      description: menu.description,
    };
    return nyobj;
  });
  console.log("ini dari var mapped menu: ", mappedMenu);
  form.setFieldsValue({
    menu: menu.menu,
  });
  useEffect(() => {
    try {
      axios
        .get("http://18.141.224.137:3000/menu")
        .then((res) => setMenu(res.data.items));
    } catch (e) {
      console.log(e.message);
    }
  }, []);
  console.log("ini dari state menu ", menu);

  const onFinish = (value) => {
    console.log(value);
  };

  return (
    <div>
      <Form onFinish={onFinish} form={form}>
        <Form.Item label="Category" name="category">
          <Input />
        </Form.Item>
        <Form.Item label="Menu Name" name="menu">
          <Input />
        </Form.Item>
        <Form.Item label="Price" name="price">
          <Input />
        </Form.Item>
        <Form.Item label="Description" name="description">
          <Input />
        </Form.Item>
        <Form.Item>
          <Button htmlType="submit">Submit</Button>
        </Form.Item>
      </Form>
      <ol>
        {menu.map((menu) => (
          <li>{menu.menu}</li>
        ))}
      </ol>
    </div>
  );
};

export default EditForm;
