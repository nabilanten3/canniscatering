import { Layout, Menu } from "antd";
const { Header, Content, Footer, Sider } = Layout;
const AdminFooter = () => {
	return (
		<Footer
			style={{
				textAlign: "center",
			}}
		>
			Cannis Catering ©2022
		</Footer>
	);
};

export default AdminFooter;
