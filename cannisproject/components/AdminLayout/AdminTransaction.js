import React, { useEffect, useState } from "react";
import { CheckOutlined, DeleteOutlined, EyeOutlined } from "@ant-design/icons";
import { Button, Table, Space, Tooltip, Select, Modal, Tag } from "antd";
const { Option } = Select;
import Image from "next/image";
import axios from "axios";

const AdminTransaction = () => {
  const [dataSouce, setDataSource] = useState([]);
  const [image, setImage] = useState();
  const [visible, setVisible] = useState(false);
  const [imageValue, setImageValue] = useState();
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });
  const columns = [
    { title: "Date", render: (value) => value.createdAt.split("T")[0] },
    {
      title: "Name",
      render: (value) => value.user.fullName,
    },
    {
      title: "Email",
      render: (value) => value.user.email,
    },
    {
      title: "Phone Number",
      render: (value) => value.user.phone,
    },
    { title: "Menu", render: (value) => value.menu.menu },

    {
      title: "Payment Proof",
      render: (value) => (
        <>
          <Tooltip title="Show Payment Proof">
            <Button type="text" onClick={() => handlePaymentProof(value)}>
              <Tag color="gold">Show Detail</Tag>
            </Button>
          </Tooltip>
        </>
      ),
    },
    {
      title: "Payment Status",
      render: (value) =>
        value.paymentStatus ? (
          <Tag color="green">Confirmed</Tag>
        ) : (
          <Tag color="volcano">Pending</Tag>
        ),
    },
    {
      title: "Delivery Status",
      render: (value) => {
        if (value.deliveryStatus === "0") {
          return <Tag color="volcano">Waiting For Payment</Tag>;
        } else if (value.deliveryStatus === "1") {
          return <Tag color="gold">On Delivery</Tag>;
        } else {
          return <Tag color="lime">Delivered</Tag>;
        }
      },
    },
    {
      title: "Action",
      render: (value) => (
        <>
          <Space>
            {value.paymentStatus ? null : (
              <Tooltip title="Approve Payment">
                <Button
                  icon={<CheckOutlined />}
                  type="link"
                  onClick={() => handleConfirm(value)}
                />
              </Tooltip>
            )}
            <Tooltip title="Delete Transaction">
              <Button
                type="text"
                danger
                onClick={() => handleDelete(value)}
                icon={<DeleteOutlined />}
              />
            </Tooltip>
          </Space>
        </>
      ),
    },
  ];

  const handlePaymentProof = async (value) => {
    console.log(value, "dari payment PRoof");
    setVisible(true);
    setImageValue(value.image);
  };

  const getTransactionData = async (params = {}) => {
    let response = await axios.get(
      `http://cannis-catering.online:3000/transactions`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      }
    );
    setDataSource(response.data.items);
    console.log(response.data.items);
    setPagination({
      ...params.pagination,
    });
  };

  const handleDeliveryStatus = async (value) => {
    const deliveryStatus = {
      deliveryStatus: "2",
    };
    await axios
      .put(
        `http://cannis-catering.online:3000/transactions/admin/${value.id}`,
        deliveryStatus,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      )
      .then((res) => console.log(res));
    getTransactionData();
  };

  const handleConfirm = async (value) => {
    const confirmingPayment = {
      statusPayment: true,
      deliveryStatus: "1",
    };
    await axios
      .put(
        `http://cannis-catering.online:3000/transactions/admin/${value.id}`,
        confirmingPayment,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      )
      .then((res) => console.log(res));
    console.log(value);
    getTransactionData();
  };

  const handleDelete = async (value) => {
    console.log(value, "dari handle delete");
    await axios
      .delete(`http://cannis-catering.online:3000/transactions/${value.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      })
      .then((res) => console.log(res));
    getTransactionData();
  };

  const handleTableChange = (newPagination, filters, sorter) => {
    getTransactionData({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination: newPagination,
      ...filters,
    });
  };

  useEffect(() => {
    getTransactionData({
      pagination,
    });
  }, []);
  return (
    <div>
      <Table
        dataSource={dataSouce}
        columns={columns}
        pagination={pagination}
        onChange={handleTableChange}
      />
      <Modal
        visible={visible}
        onOk={() => setVisible(false)}
        centered
        closable
        onCancel={() => setVisible(false)}
      >
        <Image
          width={500}
          height={500}
          src={`http://cannis-catering.online:3000/file/${imageValue}`}
        />
      </Modal>
    </div>
  );
};

export default AdminTransaction;
