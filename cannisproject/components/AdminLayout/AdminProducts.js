import React, { useEffect, useState, useRef } from "react";
import { Button, Space, Input, Table, Row, Col, Modal, message } from "antd";
import { DeleteFilled, EditFilled, SearchOutlined } from "@ant-design/icons";
import axios from "axios";
import FormAddProduct from "./FormAddProduct";
import { useForm } from "antd/lib/form/Form";
import FormEditProduct from "./FormEditProduct";
import Highlighter from "react-highlight-words";

const AdminProducts = () => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div
        style={{
          padding: 8,
        }}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 90,
            }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({
                closeDropdown: false,
              });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns = (showModalEdit, showModalDelete) => {
    return [
      // {
      //   title: "Image",
      //   dataIndex: "image",
      //   render: (value) => <img width={'10%'} src={appConfig.apiUrl + '/file/' + value} />,
      // },
      {
        title: "Category",
        dataIndex: "category",
        render: (value) => value.category,
        filters: [
          {
            text: "Main Menu",
            value: "Main Menu",
          },
          {
            text: "Snacks",
            value: "Snack",
          },
        ],
        onFilter: (value, record) =>
          record.category.category.indexOf(value) === 0,
        // width: "10%",
      },
      {
        title: "Menu",
        dataIndex: "menu",
        ...getColumnSearchProps("menu"),
        width: "15%",
      },
      {
        title: "Price",
        dataIndex: "price",
        defaultSortOrder: "descend",
        sorter: (a, b) => a.price - b.price,
        width: "10%",
      },
      {
        title: "Description",
        dataIndex: "description",
        // width: "60%",
      },
      {
        title: "Action",
        key: "action",
        render: (record) => {
          return (
            <>
              <Button
                type="link"
                icon={<EditFilled />}
                onClick={() => showModalEdit(record)}
              />
              <Button
                type="text"
                danger
                icon={<DeleteFilled />}
                onClick={() => showModalDelete(record)}
              />
            </>
          );
        },
        width: "10%",
      },
    ];
  };

  const [modalDataEdit, setModalDataEdit] = useState({}); //EDIT SESSION
  const [editProductModal, setEditProductModal] = useState(false); //EDIT SESSION
  const [modalIdDelete, setModalIdDelete] = useState(""); //DELETE SESSION
  const [deleteProductModal, setDeleteProductModal] = useState(false); //DELETE SESSION
  const [AddProductModal, setAddProductModal] = useState(false); //ADD SESSION
  const [dataSource, setDataSource] = useState(); //DATA SOURCE
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });
  const [formEdit] = useForm(); //PEMBEDA TIAP FORM
  const [formAdd] = useForm();
  const getMenu = async (params = {}) => {
    try {
      let response = await axios.get("http://cannis-catering.online:3000/menu");
      setDataSource(response.data.items);
      setPagination({
        ...params.pagination,
      });
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    getMenu({
      pagination,
    });
  }, []);

  const handleTableChange = (newPagination, filters, sorter) => {
    getMenu({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination: newPagination,
      ...filters,
    });
  };

  const handleCloseModal = () => {
    setAddProductModal(false);
    setDeleteProductModal(false);
    setEditProductModal(false);
  };

  //ADD SESSION
  const handleAddProduct = () => {
    setAddProductModal(true);
  };

  //DELETE SESSION

  const showModalDelete = (record) => {
    setDeleteProductModal(true);
    setModalIdDelete(record.id);
  };

  const handleOkModalDelete = async () => {
    try {
      setDeleteProductModal(false);
      await axios
        .delete(`http://cannis-catering.online:3000/menu/${modalIdDelete}`, { headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` } })
        .then((res) => message.success("success delete"));
      console.log(modalIdDelete);
      getMenu();
    } catch (err) {
      message.error("failed delete");
    }
  };

  //EDIT PRODUCT SESSION

  const [menu, setMenu] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");

  const onChangeCategory = (e) => {
    setCategory(e);
    console.log(e);
  };
  const onChangeMenu = (e) => {
    setMenu(e.target.value);
  };
  const onChangePrice = (e) => {
    setPrice(parseInt(e.target.value));
  };
  const onChangeDescription = (e) => {
    setDescription(e.target.value);
  };

  const showModalEdit = (record) => {
    setEditProductModal(true);
    setModalDataEdit(record);
    formEdit.setFieldsValue({
      category: record.category.id,
      menu: record.menu,
      price: record.price,
      description: record.description,
    });
  };

  const onFinishEdit = async () => {
    try {
      const data = formEdit.getFieldsValue();
      await axios
        .put(
          `http://cannis-catering.online:3000/menu/edit/${modalDataEdit.id}`,
          {
            ...data,
            category_id: data.category,
          },
          { headers: { "content-type": "application/json", Authorization: `Bearer ${localStorage.getItem("access_token")}` } }
        )
        .then(() => {
          message.success("success edit data");
          setEditProductModal(false);
          setModalDataEdit({});
          getMenu();
        });
    } catch (e) {
      e.message;
    }
  };

  return (
    <>
      <Row justify="end" className="my-5">
        <Col>
          <Button onClick={handleAddProduct}>+ Add Product</Button>
        </Col>
      </Row>
      <Table
        columns={columns(showModalEdit, showModalDelete)}
        dataSource={dataSource}
        pagination={pagination}
        onChange={handleTableChange}
      />
      <Modal
        visible={AddProductModal}
        onOk={handleCloseModal}
        onCancel={handleCloseModal}
        title="Add New Product"
        okText="Save"
      >
        <FormAddProduct addForm={{ getMenu, formAdd }} />
      </Modal>

      <Modal
        visible={deleteProductModal}
        onOk={handleOkModalDelete}
        onCancel={handleCloseModal}
        title="Delete Product"
        okText="Delete"
        okType="danger"
      >
        <p>are You Sure Want to delete This Menu?</p>
      </Modal>

      <Modal
        visible={editProductModal}
        onOk={onFinishEdit}
        onCancel={handleCloseModal}
        title="Edit Product"
        okText="Save Changes"
      >
        <FormEditProduct
          editForm={{
            formEdit,
            onFinishEdit,
            onChangeCategory,
            onChangeMenu,
            onChangePrice,
            onChangeDescription,
            showModalEdit,
            menu,
            category,
            price,
            description,
          }}
        />
      </Modal>
    </>
  );
};

export default AdminProducts;
