import { Button, Form, Input, Alert, Select, message } from "antd";
import TextArea from "antd/lib/input/TextArea";
const { Option } = Select;
import axios from "axios";
import React, { useState } from "react";
import appConfig from "../../config/app";
import UploadProductImage from "./UploadProductImage";

const FormAddProduct = ({ addForm }) => {
  const [menu, setMenu] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");

  const onFinishAdd = async () => {
    try {
      const newMenu = {
        category_id: category,
        menu: menu,
        price: price,
        description: description,
        image: image,
      };
      await axios
        .post(`${appConfig.apiUrl}/menu/create`, newMenu, {
          headers: { "content-type": "application/json", Authorization: `Bearer ${localStorage.getItem("access_token")}` },
        })
        .then((res) => {
          addForm.getMenu();
          setMenu("");
          message.success("berhasil Menambah Product");
        });
    } catch (e) {
      message.error(e.message);
    }
  };

  const onChangeCategory = (e) => {
    setCategory(e);
    console.log(category);
  };
  const onChangeMenu = (e) => {
    setMenu(e.target.value);
  };
  const onChangePrice = (e) => {
    setPrice(parseInt(e.target.value));
  };
  const onChangeDescription = (e) => {
    setDescription(e.target.value);
  };

  const handleChangeImage = (filepath) => {
    console.log(filepath, "isi dari handleChangeImage");
    setImage(filepath);
  };

  return (
    <div className="my-5">
      <Form onFinish={onFinishAdd} layout="vertical">
        <Form.Item name="category" label="Category">
          <Select value={category} onChange={onChangeCategory}>
            <Option value="729006e4-0a9b-40a5-8e65-e0bab714c1ac">
              Main Menu
            </Option>
            <Option value="c4bef283-49dd-4081-9c61-88a45666495f">Snacks</Option>
          </Select>
        </Form.Item>
        <Form.Item name="menu" label="Menu">
          <Input value={menu} onChange={onChangeMenu} />
        </Form.Item>
        <Form.Item name="price" label="Price">
          <Input
            value={parseInt(price)}
            onChange={onChangePrice}
            type="number"
          />
        </Form.Item>
        <Form.Item name="description" label="Description">
          <TextArea value={description} onChange={onChangeDescription} />
        </Form.Item>
        <Form.Item name="uploadPhoto" label="Upload Photo">
          <UploadProductImage onChangeImage={handleChangeImage} />
        </Form.Item>
        <Form.Item>
          <Button htmlType="submit">Add</Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default FormAddProduct;
