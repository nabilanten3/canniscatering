import { Button, Table, Modal, Space, Input } from "antd";
import React, { useEffect, useRef, useState } from "react";
import { DeleteFilled, SearchOutlined } from "@ant-design/icons";
import Highlighter from "react-highlight-words";
import axios from "axios";

const AdminUsers = () => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div
        style={{
          padding: 8,
        }}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 90,
            }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({
                closeDropdown: false,
              });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns = (showUserDelete) => {
    return [
      {
        title: "Name",
        dataIndex: "fullName",
        key: "fullName",
        width: "15%",
        ...getColumnSearchProps("fullName"),
      },
      {
        title: "Email",
        dataIndex: "email",
        key: "email",
        width: '15%'
      },
      {
        title: "Phone Number",
        dataIndex: "phone",
        key: "phone",
        width: '15%'
      },
      {
        title: "Address",
        dataIndex: "address",
        key: "address",
      },
      {
        title: "Action",
        key: "action",
        width: "5%",
        render: (record) => {
          return (
            <Button
              danger
              icon={<DeleteFilled />}
              type="link"
              onClick={() => showUserDelete(record)}
            />
          );
        },
      },
    ];
  };

  const [showDeleteModal, setDeleteModal] = useState(false);
  const [userId, setUserId] = useState("");
  const [users, setUsers] = useState([]);

  const getUsers = async (params = {}) => {
    let response = await axios.get("http://cannis-catering.online:3000/users", { headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` } });
    console.log(response.data.items);
    setUsers(response.data.items);
    setPagination({
      ...params.pagination,
    });
  };

  useEffect(() => {
    getUsers({ pagination });

  }, []);

  const handleTableChange = (newPagination, filters, sorter) => {
    getUsers({
      sortField: sorter.field,
      sortOrder: sorter.order,
      pagination: newPagination,
      ...filters,
    });
  };

  const showUserDelete = (record) => {
    setDeleteModal(true);
    setUserId(record.id);
    console.log("ini punya userId " + userId);
  };
  const handleCloseModal = () => {
    setDeleteModal(false);
  };

  const handleOk = () => {
    try {
      setDeleteModal(false);
      axios
        .delete(`http://cannis-catering.online:3000/users/${userId}`, { headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` } })
        .then((res) => console.log(res))
        .then(getUsers())
    } catch (e) {
      e.message;
    }
  };

  return (
    <>
      <Table
        dataSource={users}
        columns={columns(showUserDelete)}
        pagination={pagination}
        onChange={handleTableChange}
        className="my-5"
      />
      <Modal
        title="Delete User"
        visible={showDeleteModal}
        onOk={handleOk}
        onCancel={handleCloseModal}
        okType="danger"
        width={300}
      />
    </>
  );
};

export default AdminUsers;
