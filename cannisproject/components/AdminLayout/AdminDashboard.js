import { Card, Col, Row, Statistic } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";

const AdminDashboard = () => {

    const [users, setUsers] = useState()
    const [menu, setMenu] = useState()
    const [transaction, setTransaction] = useState()

    const getSummaryUsers = async () => {
        await axios.get("http://cannis-catering.online:3000/users", { headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` } })
            .then(res => setUsers(res.data.items.length))
    }

    const getSummaryMenu = async () => {
        await axios.get("http://cannis-catering.online:3000/menu")
            .then(res => setMenu(res.data.items.length))
    }

    const getSummaryTransaction = async () => {
        await axios.get("http://cannis-catering.online:3000/transactions", { headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` } })
            .then(res => setTransaction(res.data.items.length))
    }

    useEffect(() => {
        getSummaryUsers()
        getSummaryMenu()
        getSummaryTransaction()
    }, [])
    return (
        <div className="my-14">
            <Row justify="space-evenly">
                <Col>
                    <Card
                        hoverable
                        className="shadow-lg"
                        style={{
                            width: 300,
                        }}
                        title="Total Pengguna"
                    >
                        <Statistic title="Pengguna Aktif" valueStyle={{
                            color: '#3f8600',
                        }} value={`${users} Pengguna`} />
                    </Card>
                </Col>
                <Col>
                    <Card
                        hoverable
                        className="shadow-lg"
                        style={{
                            width: 300,
                        }}
                        title="Total Transaksi"
                    >
                        <Statistic title="Pengguna Aktif" valueStyle={{
                            color: '#3f8600',
                        }} value={`${transaction} Transaksi`} />
                    </Card>
                </Col>
                <Col>
                    <Card
                        hoverable
                        className="shadow-lg"
                        style={{
                            width: 300,
                        }}
                        title="Total Menu"
                    >
                        <Statistic title="Menu Minggu ini" valueStyle={{
                            color: '#3f8600',
                        }} value={`${menu} menu`} />
                    </Card>
                </Col>
            </Row>
        </div>
    );
};

export default AdminDashboard;
