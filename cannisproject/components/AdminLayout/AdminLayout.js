import React from "react";
import AdminHeader from "./AdminHeader";
import AdminSider from "./AdminSider";
import AdminFooter from "./AdminFooter";
import { Layout } from "antd";
const { Content } = Layout;
// import "antd/dist/antd.variable.css";

const AdminLayout = ({ children }) => (
  <Layout>
    <AdminSider />
    <Layout className="h-screen mx-5 my-2 ">
      <Content>
        <AdminHeader />
        {children}
      </Content>
    </Layout>
  </Layout>
);

export default AdminLayout;
