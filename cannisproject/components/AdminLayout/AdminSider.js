import {
  WalletFilled,
  ShoppingFilled,
  HomeFilled,
  UserOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Button, Layout, Menu } from "antd";
import React, { useEffect, useState } from "react";
const { Sider } = Layout;
import Link from "next/link";
import { useRouter } from "next/router";

const AdminSider = () => {
  const [collapsed, setCollapsed] = useState(false);
  const router = useRouter();
  const [selectedKeys, setSelectedKeys] = useState('');

  useEffect(() => {
    const pathNow = router.pathname.split("/")[2]
    setSelectedKeys(pathNow)
    console.log("masuk sin")
  }, [router.pathname])

  const handleLogout = () => {
    localStorage.removeItem("access_token");
    setTimeout(() => {
      router.push("/")
    }, 1000)

  };
  function getItem(label, key, icon) {
    return { key, icon, label };
  }
  const items = [
    getItem(
      <Link href={"/admin/dashboard"}>Home</Link>,
      "dashboard",
      <HomeFilled />
    ),
    getItem(
      <Link href={"/admin/listUsers"}>Users</Link>,
      "listUsers",
      <UserOutlined />
    ),
    getItem(
      <Link href={"/admin/products"}>Products</Link>,
      "products",
      <ShoppingFilled />
    ),
    getItem(
      <Link href={"/admin/transaction"}>Transaction</Link>,
      "transaction",
      <WalletFilled />
    ),
    getItem(
      <Button type="text" danger icon={<LogoutOutlined style={{ marginRight: 20, marginLeft: -15 }} />} onClick={handleLogout}>
        Logout
      </Button>,
      "logout",

    ),
  ];

  return (
    <Sider
      theme="light"
      collapsible
      collapsed={collapsed}
      onCollapse={(value) => setCollapsed(value)}
      style={{ background: "white" }}
    >
      {console.log(selectedKeys, " apa isi selected key")}
      <Menu
        theme="light"
        defaultSelectedKeys={'home'}
        selectedKeys={[selectedKeys]}
        mode="inline"
        items={items}
        style={{ marginTop: 50 }}
        onClick={(arg) => setSelectedKeys(arg.key)}
      />
    </Sider>
  );
};

export default AdminSider;
