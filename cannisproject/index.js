import { Layout, ConfigProvider } from "antd";
const { Header, Sider, Content } = Layout;
import React from "react";
import "tailwindcss/tailwind.css";
import "antd/dist/antd.variable.css";
import SideBar from "./components/CustomerLayout/userProfile/SiderBar";
import ContentBar from "./components/CustomerLayout/userProfile/ContentBar";
ConfigProvider.config({
  theme: {
    primaryColor: "#95CB44",
  },
});

const userProfile = () => {
  return (
    <>
      <Layout
        style={{
          minHeight: "100vh",
        }}
      >
        <Layout className="site-layout">
          <SideBar />

          <Content
            style={{
              margin: "20px 16px",
            }}
          >
            <ContentBar />
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default userProfile;
