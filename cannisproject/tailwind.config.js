/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        mainColor: "#95CB44",
        fontColor: "#375A03",
      },
      backgroundImage: "url('/public/assets/LandingBackground.png')",
    },
  },
  plugins: [],
};
