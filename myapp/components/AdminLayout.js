import AdminContent from './AdminContent'
import AdminFooter from './AdminFooter'
import AdminHeader from './AdminHeader'
import Navigation from './AdminNavigation'
import {Layout} from 'antd'

function AdminLayout({children}) {
    return (
      <>
      <Layout>
        <AdminHeader/>
        <Layout>
            <Navigation/>
            <Layout>
                <AdminContent>
                    {children}
                </AdminContent>
                <AdminFooter/>
            </Layout>
        </Layout>
      </Layout>
      
    
    
    </>
  )
}

export default AdminLayout