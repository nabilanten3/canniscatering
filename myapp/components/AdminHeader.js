import { Layout } from 'antd';
const { Header } = Layout;
function AdminHeader() {
  return (
    <Header
        className="site-layout-sub-header-background"
        style={{
          padding: 0,
        }}
      />
  )
}

export default AdminHeader