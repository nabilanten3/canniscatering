import { Layout } from 'antd';
const { Content } = Layout;
function AdminContent({children}) {
  return (
    <Content
        style={{
          margin: '24px 16px 0',
        }}
      >
        <div
          className="site-layout-background"
          style={{
            padding: 24,
            minHeight: 360,
          }}
        >
            {children}
        </div>
      </Content>
  )
}

export default AdminContent